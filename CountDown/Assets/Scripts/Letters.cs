using System.Collections.Generic;
using System.Text;
using UnityEngine;

/// <summary>
/// this component will generate and mantain the list of available letters and check if they are still consumable when a word is submitted by the player
/// </summary>
public class Letters : MonoBehaviour
{

    [SerializeField]
    List<char> mLetterList = new List<char>();
    List<char> mConsumableLetterList = new List<char>();

    [SerializeField]
    string alphabet = "abcdefghijklmnopqrstuvwxyz";
    char[] mAlphabetArray;

    public static Letters instance;
    void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);

        mAlphabetArray = alphabet.ToCharArray();
    }

    public void generateLetters(int amount)
    {
        mLetterList.Clear();
        mConsumableLetterList.Clear();
        for (int i = 0; i < amount; i++)
        {
            char randomized = mAlphabetArray[Random.Range(0, alphabet.Length)];
            mLetterList.Add(randomized);
            mConsumableLetterList.Add(randomized);
        }
    }

    /// <summary>
    /// Since it's not specified what should happen in the UI when the available letters are used for a word, I'll keep returning them in
    /// the result of this function, also because the examples made me think that for the player to mistakenly use again letters already
    /// consumed is an intended difficulty of the game.
    /// </summary>
    /// <returns></returns>
    public string getLetters()
    {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < mLetterList.Count; i++)
        {
            result.Append(mLetterList[i]);
        }
        return result.ToString();
    }

    public bool consumeLetters(string word)
    {
        char[] wordArray = word.ToCharArray();
        for (int i = 0; i < wordArray.Length; i++)
        {
            if (!mConsumableLetterList.Contains(wordArray[i])) { return false; }
        }

        for (int i = 0; i < wordArray.Length; i++)
        {
            mConsumableLetterList.Remove(wordArray[i]);
        }

        return true;
    }

}
