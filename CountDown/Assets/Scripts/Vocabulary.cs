using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// this component will hold the vocabulary data, change the vocabulary path to change the set of words
/// </summary>
public class Vocabulary : MonoBehaviour
{

    [SerializeField]
    string vocabularyPath = "";

    List<string> mVocabularyData = new List<string>();

    public static Vocabulary instance;
    void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);

        string[] Buffer = System.IO.File.ReadAllLines(vocabularyPath);
        for (int i = 0; i < Buffer.Length; i++)
        {
            mVocabularyData.Add(Buffer[i]);
        }
    }

    public bool isWordLegit(string word) { return mVocabularyData.Contains(word); }
}
