using UnityEngine;
using System.Collections;

/// <summary>
/// Empty anagram game.
/// </summary>
public class EmptyGame : IGame 
{
	public void SubmitWord(string word)
	{
		Debug.Log("Submitting word " + word);
	}

	public string GetWordEntryAtPosition(int position)
	{
		return null;
	}

	public int GetScoreAtPosition(int position)
	{
		return -1;
	}

	public string GetAvailableLetters ()
	{
		return "AvailableCharactersNotImplementedYet";
	}

	public void Reset ()
	{
		throw new System.NotImplementedException ();
	}
}
