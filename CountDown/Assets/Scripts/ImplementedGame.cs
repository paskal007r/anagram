/// <summary>
/// implemented anagram game
/// </summary>
public class ImplementedGame : IGame
{

    /*
     Since nowhere in the instructions the number of available letters is specified I intend to make it a customizable parameter
     In the GameUI script there is an availableLeters variable that specifies that number but to access directly that variable would require to 
     violate the independence of the two classes wich is the whole point of the IGame interface. I'd recommend to change the interface specification
     so that there is a way to set this number from the GameUI or, even better, to remove that variable from the GameUI and place it in this class
     */
    public int availableLeters = 15;

    public ImplementedGame()
    {
        Letters.instance.generateLetters(availableLeters);
    }

    public string GetAvailableLetters()
    {
        return Letters.instance.getLetters();
    }

    public int GetScoreAtPosition(int position)
    {
        return WordList.instance.getScoreAt(position);
    }

    public string GetWordEntryAtPosition(int position)
    {
        return WordList.instance.getWordAt(position);
    }

    public void Reset()
    {
        Letters.instance.generateLetters(availableLeters);
        WordList.instance.Restart();
    }

    public void SubmitWord(string word)
    {
        if (Vocabulary.instance.isWordLegit(word))
            if (Letters.instance.consumeLetters(word))
                WordList.instance.addWord(word);
    }



}
