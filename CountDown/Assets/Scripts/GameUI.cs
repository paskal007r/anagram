using UnityEngine;

public class GameUI : MonoBehaviour
{
    public int availableLeters = 15;
    private string mTextInput = "Add your word here";
    private IGame mInterface;
    private void Start()
    {
        // Initialise interface
        mInterface = new ImplementedGame();
    }

    private void OnGUI()
    {
        int xPosition = 50;

        GUI.Label(new Rect(xPosition, 25, 300, 30), mInterface.GetAvailableLetters());
        mTextInput = GUI.TextField(new Rect(xPosition, 50, 150, 30), mTextInput, 25);

        if (GUI.Button(new Rect(xPosition, 100, 150, 30), "Submit word"))
        {
            if (!string.IsNullOrEmpty(mTextInput))
            {
                mInterface.SubmitWord(mTextInput);
            }
        }
        if (GUI.Button(new Rect(xPosition, 135, 150, 30), "Reset"))
        {
            mInterface.Reset();
        }

        GUI.BeginGroup(new Rect(xPosition + 200, 50, 200, 600));
        GUILayout.BeginHorizontal();

        // Score
        GUILayout.BeginVertical();

        for (int index = 0; index < 9; index++)
        {
            GUILayout.Label(mInterface.GetScoreAtPosition(index).ToString());
        }

        GUILayout.EndVertical();

        // Word
        GUILayout.BeginVertical();

        for (int index = 0; index < 9; index++)
        {
            GUILayout.Label(mInterface.GetWordEntryAtPosition(index));
        }

        GUILayout.EndVertical();

        GUILayout.EndHorizontal();
        GUI.EndGroup();
    }
}
