using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// this class will hold the list of words that were accepted in the current manche
/// </summary>
public class WordList : MonoBehaviour
{
    List<string> mWordList = new List<string>();

    public static WordList instance;
    void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);

    }
    public void addWord(string word) { mWordList.Add(word); }
    public string getWordAt(int index)
    {
        if (index < mWordList.Count)
            return mWordList[index];
        else return "";
    }
    public int getScoreAt(int index)
    {
        if (index < mWordList.Count)
            return mWordList[index].Length;
        else return 0;
    }

    public void Restart() { mWordList.Clear(); }
}
